var assert = require('assert');
var chai = require('chai');
var expect = chai.expect;
var script = require('../src/js/script')

describe('script', function(){
    describe('#generateCPUSelection()', function() {
        for (var i = 0; i < 100; i++) {
            it('should return 1 when the user beats the cpu - ' + (i + 1), function() {
                var cpuChoice = script.generateCPUSelection();
                expect(cpuChoice).to.be.above(-1);
                expect(cpuChoice).to.be.below(3);
            })
        }
    })
    describe('#checkResult(player, cpu)', function() {
        it('should return 1 when the user beats the cpu', function() {
            assert.equal(script.checkResult(1, 0), 1);
            assert.equal(script.checkResult(2, 1), 1);
            assert.equal(script.checkResult(0, 2), 1);
        })
        it('should return 0 when the values entered are the same', function() {
            assert.equal(script.checkResult(0, 0), 0);
            assert.equal(script.checkResult(1, 1), 0);
            assert.equal(script.checkResult(2, 2), 0);
        })
        it('should return -1 when the cpu beats the user', function() {
            assert.equal(script.checkResult(2, 0), -1);
            assert.equal(script.checkResult(0, 1), -1);
            assert.equal(script.checkResult(1, 2), -1);
        })
    })
    describe('#resultText(player, cpu)', function() {
        it('should return win when the parameter is 1', function() {
            assert.equal(script.resultText(1), "You win! Close this alert to play again.");
        })
        it('should return win when the parameter is 0', function() {
            assert.equal(script.resultText(0), "You drew, no winners here! Close this alert to play again.");
        })
        it('should return win when the parameter is -1', function() {
            assert.equal(script.resultText(-1), "You lose! Close this alert to play again.");
        })
    })
})
