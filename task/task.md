# Assetz Capital

Senior Software Engineer

Rock-Paper-Scissors - Technical Exercise (Pre-interview)

This exercise is to be completed prior to your interview and links to be to emailed to sue@tag-ltd.com the night before your interview. Please let me know as a matter of urgency if you have any questions about this exercise

Write a simple web application, using a JavaScript framework of your choice, where a user can play the game Rock-Paper-Scissors against the computer.  If you don’t know this game here is a link to the rules, it’s quite simple [wiki](https://en.wikipedia.org/wiki/Rock-paper-scissors)

When you come in for interview, you will be given a pair-programming exercise to add a feature to your rock-paper-scissors codebase. During this exercise we are going to analyze your code structure (JS, CSS and HTML), your problem-solving approach, and your ability to discuss your thought processes as you implement the feature.

It would be great to see some use of NPM and Gulp so that we don't have to deploy your code to a web server to run the app, and it would also be good to see some unit testing.
