// functional
function generateCPUSelection() {
    return Math.floor(Math.random()*3);
}

// Rock = 0, Paper = 1, Scissors = 2
function checkResult(player, cpu) {
    if (player == cpu) {
        return 0;
    }
    else if ((player == 0 && cpu == 2) || (player == 1 && cpu == 0) || (player == 2 && cpu == 1)) {
        return 1;
    }
    else {
        return -1;
    }
}

function resultText(result, scripts) {
    var resetScript = " <a href='#' onclick='" + scripts + "'>Clear result.</a>";
    if (result == -1) {
        return "You lose!" + resetScript;
    }
    else if (result == 0) {
        return "You drew, no winners here!" + resetScript;
    }
    else {
        return "You win!" + resetScript;
    }
}

// visual
function play(player) {
    clearResult()
    var cpu = generateCPUSelection();
    showResult(player, cpu, checkResult(player, cpu));
}

function showResult(player, cpu, result) {
    showCPU(cpu);
    toggleBorders(player, result);
    updateStatus(resultText(result, "clearResult();"));
}

function clearResult() {
    $("#cpuChoice img").attr("src","dist/images/crown.png");
    $("#cpuChoice p").html("???");
    $(".gameOption").removeClass("win");
    $(".gameOption").removeClass("draw");
    $(".gameOption").removeClass("lose");
}

function showCPU(cpu) {
    if (cpu == 0) {
        $("#cpuChoice img").attr("src","dist/images/rock.png");
        $("#cpuChoice p").html("Rock");
    }
    else if (cpu == 1) {
        $("#cpuChoice img").attr("src","dist/images/paper.png");
        $("#cpuChoice p").html("Paper");
    }
    else {
        $("#cpuChoice img").attr("src","dist/images/scissors.png");
        $("#cpuChoice p").html("Scissors");
    }
}

function toggleBorders(player, result) {
    if (result == 1) {
        $("#cpuChoice").toggleClass("lose");
        $("#userChoice" + player).toggleClass("win");
    }
    else if (result == 0) {
        $("#cpuChoice").toggleClass("draw");
        $("#userChoice" + player).toggleClass("draw");
    }
    else {
        $("#cpuChoice").toggleClass("win");
        $("#userChoice" + player).toggleClass("lose");
    }
}

function updateStatus(content) {
    $('#status').html(content);
}

// If Node,
if(typeof exports !== 'undefined') {
    exports.checkResult = checkResult;
    exports.generateCPUSelection = generateCPUSelection;
    exports.resultText = resultText;
}
