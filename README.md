# Rock Paper Scissors

Written by Matt Weeks.

## Resources used

### Icons

[Crown icon](https://img.icons8.com/plasticine/344/crown.png)
[Paper icon](https://img.icons8.com/color/344/paper.png)
[Rock icon](https://img.icons8.com/ultraviolet/344/rock.png)
[Scissors icon](https://img.icons8.com/color/344/scissors.png)
[Win icon](https://image.flaticon.com/icons/svg/152/152597.svg)
[Lose icon](https://image.flaticon.com/icons/svg/152/152596.svg)

### Libraries used

Bootstrap CSS
JQuery
Chai
Mocha

### Running the game

Can be run on live server or open 'web/index.html' directly.
